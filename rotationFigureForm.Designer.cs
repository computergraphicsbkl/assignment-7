﻿namespace Editor3D
{
    partial class RotationFigureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generatrixTextBox = new System.Windows.Forms.TextBox();
            this.generatrixLabel = new System.Windows.Forms.Label();
            this.axisLabel = new System.Windows.Forms.Label();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.splitsLabel = new System.Windows.Forms.Label();
            this.numericUpDown19 = new System.Windows.Forms.NumericUpDown();
            this.createModelButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).BeginInit();
            this.SuspendLayout();
            // 
            // generatrixTextBox
            // 
            this.generatrixTextBox.Location = new System.Drawing.Point(30, 38);
            this.generatrixTextBox.Multiline = true;
            this.generatrixTextBox.Name = "generatrixTextBox";
            this.generatrixTextBox.Size = new System.Drawing.Size(211, 132);
            this.generatrixTextBox.TabIndex = 0;
            // 
            // generatrixLabel
            // 
            this.generatrixLabel.AutoSize = true;
            this.generatrixLabel.Location = new System.Drawing.Point(13, 13);
            this.generatrixLabel.Name = "generatrixLabel";
            this.generatrixLabel.Size = new System.Drawing.Size(228, 13);
            this.generatrixLabel.TabIndex = 1;
            this.generatrixLabel.Text = "1. Задайте точки образующей  в виде X Y Z";
            // 
            // axisLabel
            // 
            this.axisLabel.AutoSize = true;
            this.axisLabel.Location = new System.Drawing.Point(16, 186);
            this.axisLabel.Name = "axisLabel";
            this.axisLabel.Size = new System.Drawing.Size(136, 13);
            this.axisLabel.TabIndex = 64;
            this.axisLabel.Text = "2. Задайте ось вращения";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(30, 212);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(32, 17);
            this.radioButton4.TabIndex = 65;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "X";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(78, 212);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(32, 17);
            this.radioButton5.TabIndex = 66;
            this.radioButton5.Text = "Y";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(126, 212);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(32, 17);
            this.radioButton6.TabIndex = 67;
            this.radioButton6.Text = "Z";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // splitsLabel
            // 
            this.splitsLabel.AutoSize = true;
            this.splitsLabel.Location = new System.Drawing.Point(19, 245);
            this.splitsLabel.Name = "splitsLabel";
            this.splitsLabel.Size = new System.Drawing.Size(185, 13);
            this.splitsLabel.TabIndex = 68;
            this.splitsLabel.Text = "3. Укажите количество разбиений:";
            // 
            // numericUpDown19
            // 
            this.numericUpDown19.Location = new System.Drawing.Point(30, 270);
            this.numericUpDown19.Name = "numericUpDown19";
            this.numericUpDown19.Size = new System.Drawing.Size(43, 20);
            this.numericUpDown19.TabIndex = 69;
            // 
            // createModelButton
            // 
            this.createModelButton.Location = new System.Drawing.Point(88, 299);
            this.createModelButton.Name = "createModelButton";
            this.createModelButton.Size = new System.Drawing.Size(100, 23);
            this.createModelButton.TabIndex = 70;
            this.createModelButton.Text = "Создать модель";
            this.createModelButton.UseVisualStyleBackColor = true;
            this.createModelButton.Click += new System.EventHandler(this.createModelButton_Click);
            // 
            // rotationFigure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 331);
            this.Controls.Add(this.createModelButton);
            this.Controls.Add(this.numericUpDown19);
            this.Controls.Add(this.splitsLabel);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton6);
            this.Controls.Add(this.axisLabel);
            this.Controls.Add(this.generatrixLabel);
            this.Controls.Add(this.generatrixTextBox);
            this.Name = "rotationFigure";
            this.Text = "rotationFigure";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown19)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox generatrixTextBox;
        private System.Windows.Forms.Label generatrixLabel;
        private System.Windows.Forms.Label axisLabel;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Label splitsLabel;
        private System.Windows.Forms.NumericUpDown numericUpDown19;
        private System.Windows.Forms.Button createModelButton;
    }
}