﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;

namespace Editor3D
{
    public partial class PlotForm : Form
    {
        public PlotForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Visible = false;
        }
    }
}