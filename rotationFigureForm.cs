﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor3D
{
    public partial class RotationFigureForm : Form
    {
        MainForm mainForm;

        public RotationFigureForm()
        {
            InitializeComponent();
            
        }

        public RotationFigureForm(MainForm mainForm)
        {
            InitializeComponent();
            this.mainForm = mainForm;
        }

        private void createModelButton_Click(object sender, EventArgs e)
        {
            List<Point3D> points = new List<Point3D>();
            string[] lines = generatrixTextBox.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            
            foreach (string line in lines)
            {
                string[] strValues = line.Split();
                double x = Convert.ToDouble(strValues[0]);
                double y = Convert.ToDouble(strValues[1]);
                double z = Convert.ToDouble(strValues[2]);
                points.Add(new Point3D(x, y, z));
            }

            int splits = (int) numericUpDown19.Value;
            double angle = (360d / splits) * Math.PI / 180;

            Transformation matrix = Transformation.RotateX(angle);
            if (radioButton5.Checked)
                matrix = Transformation.RotateY(angle);
            else if (radioButton6.Checked)
                matrix = Transformation.RotateZ(angle);

            mainForm.DrawRotationFigure(points, matrix, splits);
            this.Close();
        }
    }
}
