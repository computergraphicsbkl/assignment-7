﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using org.mariuszgromada.math.mxparser;

namespace Editor3D
{
    interface IPrimitive
    {
        void Draw(Graphics g, Transformation projection, int width, int height);
        void Apply(Transformation t);
    }
    
    public class Point3D : IPrimitive
    {
        private static double POINT_SIZE = 6;
        private double[] coords = new double[] { 0, 0, 0, 1 };
        private Color color = Color.Black;

        public double X
        {
            get => coords[0];
            set => coords[0] = value;
        }

        public double Y
        {
            get => coords[1];
            set => coords[1] = value;
        }

        public double Z
        {
            get => coords[2];
            set => coords[2] = value;
        }

        public double K
        {
            get => coords[3];
            set => coords[3] = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public string[] ToStringArray()
        {
            string[] res = new string[5];
            res[0] = "v";
            res[1] = X.ToString();
            res[2] = Y.ToString();
            res[3] = Z.ToString();
            res[4] = K.ToString();
            return res;
        }

        public override string ToString()
        {
            return String.Format("x={0} y={1} z={2} k={3}", X, Y, Z, K);
        }

        public Point3D() { }

        public Point3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        
        public Point3D(double x, double y, double z, Color color)
        {
            X = x;
            Y = y;
            Z = z;
            this.color = color;
        }

        public Point3D(Point3D point)
        {
            X = point.X;
            Y = point.Y;
            Z = point.Z;
            color = point.color;
        }

        private Point3D(double[] arr)
        {
            coords = arr;
        }

        public static Point3D FromPoint(Point point)
        {
            return new Point3D(point.X, point.Y, 0);
        }

        public void Apply(Transformation t)
        {
            double[] newCoords = new double[4];
            for (int i = 0; i < 4; ++i)
            {
                newCoords[i] = 0;
                for (int j = 0; j < 4; ++j)
                    newCoords[i] += coords[j] * t.Matrix[j, i];
            }
            coords = newCoords;
        }

        public Point3D Transform(Transformation t)
        {
            var p = new Point3D(X, Y, Z);
            p.Apply(t);
            return p;
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            var projected = Transform(projection).NormalizedToDisplay(width, height);
            var x = projected.X;
            var y = projected.Y;
            g.FillEllipse(new SolidBrush(color), 
                (float)(x - POINT_SIZE / 2), (float)(y - POINT_SIZE / 2),
                (float)POINT_SIZE, (float)POINT_SIZE);
        }

        /*
         * Преобразует координаты из ([-1, 1], [-1, 1], [-1, 1]) в ([0, width), [0, height), [-1, 1]).
         */
        public Point3D NormalizedToDisplay(int width, int height)
        {
            var x = (X / K + 1) / 2 * width;
            var y = (-Y / K + 1) / 2 * height;
            return new Point3D(x, y, Z);
        }
    }
    
    public class Line : IPrimitive
    {
        private Point3D a;
        private Point3D b;
        private Color color = Color.Black;

        public Point3D A
        {
            get => a;
            set => a = value;
        }
        
        public Point3D B
        {
            get => b;
            set => b = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public Line(Point3D a, Point3D b)
        {
            A = a;
            B = b;
        }
        
        public Line(Point3D a, Point3D b, Color color)
        {
            A = a;
            B = b;
            this.color = color;
        }

        public void Apply(Transformation t)
        {
            A = A.Transform(t);
            B = B.Transform(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            var c = A.Transform(projection).NormalizedToDisplay(width, height);
            var d = B.Transform(projection).NormalizedToDisplay(width, height);
            g.DrawLine(new Pen(color), (float)c.X, (float)c.Y, (float)d.X, (float)d.Y);
        }
    }

     public class Facet : IPrimitive
    {
        private IList<Point3D> points = new List<Point3D>();
        private Color color = Color.Black;

        public IList<Point3D> Points
        {
            get => points;
            set => points = value;
        }

        public Color Color
        {
            get => color;
            set => color = value;
        }

        public Facet(){}

        public Facet(IList<Point3D> points)
        {
            this.points = points;
        }
        
        public Facet(IList<Point3D> points, Color color)
        {
            this.points = points;
        }

        public void FacetAddPoint(Point3D p)
        {
            points.Add(p);
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            for (int i = 0; i < Points.Count - 1; ++i)
            {
                var line = new Line(Points[i], Points[i + 1], color);
                line.Draw(g, projection, width, height);
            }
            new Line(Points[Points.Count - 1], Points[0], color).Draw(g, projection, width, height);
        }
    }
    
    interface IPolyhedron : IPrimitive
    {
        Point3D Center { get; }
        List<Point3D> points { get; }
        List<Facet> facets { get; }
        string polyhedronName { get; }
    }
    
    class Tetrahedron : IPolyhedron
    {
        // первые три точки - основание тетраэдра, четвертая точка - его вершина
        List<Point3D> IPolyhedron.points { get => Points; }
        List<Facet> IPolyhedron.facets { get => Facets;  }
        string IPolyhedron.polyhedronName { get => PolyhedronName;  }

        public List<Point3D> Points { get; private set; } = new List<Point3D>();
        public List<Facet> Facets { get; private set; } = new List<Facet>();
        public string PolyhedronName { get; private set; } = "Tetrahedron";

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 4; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 4;
                p.Y /= 4;
                p.Z /= 4;
                return p;
            }
        }

        public Tetrahedron() { }

        public Tetrahedron(double size) 
        {
            double h = Math.Sqrt(2.0 / 3.0) * size;
            Points = new List<Point3D>();

            Points.Add(new Point3D(-size / 2, 0, h / 3));
            Points.Add(new Point3D(0, 0, -h * 2 / 3));
            Points.Add(new Point3D(size / 2, 0, h / 3));
            Points.Add(new Point3D(0, h, 0));

            // Основание тетраэдра
            Facets.Add(new Facet(new Point3D[] { Points[0], Points[1], Points[2] }));
            // Левая грань
            Facets.Add(new Facet(new Point3D[] { Points[1], Points[3], Points[0] }));
            // Правая грань
            Facets.Add(new Facet(new Point3D[] { Points[2], Points[3], Points[1] }));
            // Передняя грань
            Facets.Add(new Facet(new Point3D[] { Points[0], Points[3], Points[2] }));
        }

        public Tetrahedron(double size, Color color)
        {
            double h = Math.Sqrt(2.0 / 3.0) * size;
            Points = new List<Point3D>();

            Points.Add(new Point3D(-size / 2, 0, h / 3, color));
            Points.Add(new Point3D(0, 0, -h * 2 / 3, color));
            Points.Add(new Point3D(size / 2, 0, h / 3, color));
            Points.Add(new Point3D(0, h, 0, color));

            // Основание тетраэдра
            Facets.Add(new Facet(new Point3D[] { Points[0], Points[1], Points[2] }, color));
            // Левая грань
            Facets.Add(new Facet(new Point3D[] { Points[1], Points[3], Points[0] }, color));
            // Правая грань
            Facets.Add(new Facet(new Point3D[] { Points[2], Points[3], Points[1] }, color));
            // Передняя грань
            Facets.Add(new Facet(new Point3D[] { Points[0], Points[3], Points[2] }, color));
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            foreach (var facet in Facets)
                facet.Draw(g, projection, width, height);
        }

    }
    
    class Icosahedron : IPolyhedron
    {
        // кол-во вершин = 12
        // кол-во граней = 20
        List<Point3D> IPolyhedron.points { get => Points; }
        List<Facet> IPolyhedron.facets { get => Facets; }
        string IPolyhedron.polyhedronName { get => PolyhedronName; }

        public List<Point3D> Points { get; private set; } = new List<Point3D>();
        public List<Facet> Facets { get; private set; } = new List<Facet>();
        public string PolyhedronName { get; private set; } = "Icosahedron";

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 12; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 12;
                p.Y /= 12;
                p.Z /= 12;
                return p;
            }
        }

        public Icosahedron() { }

        public Icosahedron(double size)
        {
            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            Points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                Points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i)));
                Points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10)));
            }

            Points.Add(new Point3D(0, R, 0));
            Points.Add(new Point3D(0, -R, 0));

            // середина
            for (int i = 0; i < 10; ++i)
                Facets.Add(new Facet(new Point3D[] { Points[i], Points[(i + 1) % 10], Points[(i + 2) % 10] }));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                Facets.Add(new Facet(new Point3D[] { Points[2 * i], Points[10], Points[(2 * (i + 1)) % 10] }));
                // нижняя часть
                Facets.Add(new Facet(new Point3D[] { Points[2 * i + 1], Points[11], Points[(2 * (i + 1) + 1) % 10] }));
            }
        }

        
        public Icosahedron(double size, Color color)
        {
            // радиус описанной сферы
            double R = (size * Math.Sqrt(2.0 * (5.0 + Math.Sqrt(5.0)))) / 4;

            // радиус вписанной сферы
            double r = (size * Math.Sqrt(3.0) * (3.0 + Math.Sqrt(5.0))) / 12;

            Points = new List<Point3D>();

            for (int i = 0; i < 5; ++i)
            {
                Points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i),
                    R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i), color));
                Points.Add(new Point3D(
                    r * Math.Cos(2 * Math.PI / 5 * i + 2 * Math.PI / 10),
                    -R / 2,
                    r * Math.Sin(2 * Math.PI / 5 * i + 2 * Math.PI / 10), color));
            }

            Points.Add(new Point3D(0, R, 0, color));
            Points.Add(new Point3D(0, -R, 0, color));

            // середина
            for (int i = 0; i < 10; ++i)
                Facets.Add(new Facet(new Point3D[] { Points[i], Points[(i + 1) % 10], Points[(i + 2) % 10] }, color));

            for (int i = 0; i < 5; ++i)
            {
                // верхняя часть
                Facets.Add(new Facet(new Point3D[] { Points[2 * i], Points[10], Points[(2 * (i + 1)) % 10] }, color));
                // нижняя часть
                Facets.Add(new Facet(new Point3D[] { Points[2 * i + 1], Points[11], Points[(2 * (i + 1) + 1) % 10] },
                           color));
            }
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            if (Points.Count != 12) return;
            
            foreach (var facet in Facets)
                facet.Draw(g, projection, width, height);
        }
    }
    
    class Dodecahedron : IPolyhedron
    {
        // кол-во вершин = 20
        List<Point3D> IPolyhedron.points { get => Points; }
        List<Facet> IPolyhedron.facets { get => Facets; }
        string IPolyhedron.polyhedronName { get => PolyhedronName; }

        public List<Point3D> Points { get; private set; } = new List<Point3D>();
        public List<Facet> Facets { get; private set; } = new List<Facet>();
        public string PolyhedronName { get; private set; } = "Dodecahedron";

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < 20; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= 20;
                p.Y /= 20;
                p.Z /= 20;
                return p;
            }
        }

        public Dodecahedron() { }

        public Dodecahedron(double size)
        {
            Icosahedron icosahedron = new Icosahedron(size);

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.Facets[i].Points[0].X
                          + icosahedron.Facets[i].Points[1].X
                          + icosahedron.Facets[i].Points[2].X) / 3;
                double y = (icosahedron.Facets[i].Points[0].Y
                          + icosahedron.Facets[i].Points[1].Y
                          + icosahedron.Facets[i].Points[2].Y) / 3;
                double z = (icosahedron.Facets[i].Points[0].Z
                          + icosahedron.Facets[i].Points[1].Z
                          + icosahedron.Facets[i].Points[2].Z) / 3;
                Points.Add(new Point3D(x, y, z));
            }
            initFacets();
        }

        public Dodecahedron(double size, Color color)
        {
            Icosahedron icosahedron = new Icosahedron(size);

            for (int i = 0; i < 20; i++)
            {
                double x = (icosahedron.Facets[i].Points[0].X
                          + icosahedron.Facets[i].Points[1].X
                          + icosahedron.Facets[i].Points[2].X) / 3;
                double y = (icosahedron.Facets[i].Points[0].Y
                          + icosahedron.Facets[i].Points[1].Y
                          + icosahedron.Facets[i].Points[2].Y) / 3;
                double z = (icosahedron.Facets[i].Points[0].Z
                          + icosahedron.Facets[i].Points[1].Z
                          + icosahedron.Facets[i].Points[2].Z) / 3;
                Points.Add(new Point3D(x, y, z, color));
            }
            initFacets();
        }

        private void initFacets()
        {
            // 12 граней
            Facets.Add(new Facet(new Point3D[] { Points[10], Points[12], Points[14], Points[16], Points[18] }));
            Facets.Add(new Facet(new Point3D[] { Points[11], Points[13], Points[15], Points[17], Points[19] }));
            Facets.Add(new Facet(new Point3D[] { Points[0], Points[1], Points[2], Points[12], Points[10] }));
            Facets.Add(new Facet(new Point3D[] { Points[2], Points[3], Points[4], Points[14], Points[12] }));
            Facets.Add(new Facet(new Point3D[] { Points[4], Points[5], Points[6], Points[16], Points[14] }));
            Facets.Add(new Facet(new Point3D[] { Points[6], Points[7], Points[8], Points[18], Points[16] }));
            Facets.Add(new Facet(new Point3D[] { Points[8], Points[9], Points[0], Points[10], Points[18] }));
            Facets.Add(new Facet(new Point3D[] { Points[1], Points[2], Points[3], Points[13], Points[11] }));
            Facets.Add(new Facet(new Point3D[] { Points[3], Points[4], Points[5], Points[15], Points[13] }));
            Facets.Add(new Facet(new Point3D[] { Points[5], Points[6], Points[7], Points[17], Points[15] }));
            Facets.Add(new Facet(new Point3D[] { Points[7], Points[8], Points[9], Points[19], Points[17] }));
            Facets.Add(new Facet(new Point3D[] { Points[9], Points[0], Points[1], Points[11], Points[19] }));
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            if (Points.Count != 20) return;

            foreach (Facet facet in Facets)
              facet.Draw(g, projection, width, height);
        }
    }


    public class RotationFigure : IPolyhedron
    {
        List<Point3D> IPolyhedron.points { get => Points; }
        List<Facet> IPolyhedron.facets { get => Facets; }
        string IPolyhedron.polyhedronName { get => PolyhedronName; }

        public List<Point3D> Points { get; private set; } = new List<Point3D>();
        public List<Facet> Facets { get; private set; } = new List<Facet>();
        public string PolyhedronName { get; } = "RotationFigure";

        public RotationFigure() { }

        public RotationFigure(List<Point3D> generatrix, Transformation matrix, int splits)
        {
            double angle = (360d / splits) * Math.PI / 180;
            List<Point3D> currentList = new List<Point3D>();
            Facet topFacet = new Facet();
            Facet downFacet = new Facet();

            for (int i = 0; i < generatrix.Count; i++)
            {
                currentList.Add(new Point3D(generatrix[i]));
                Points.Add(new Point3D(generatrix[i]));
            }
            topFacet.Points.Add(Points[0]);
            downFacet.Points.Add(Points[Points.Count - 1]);

            // Добавление всех граней кроме последнего слоя
            for (int i = 0; i < splits - 1; i++)
            {
                currentList[0].Apply(matrix);
                Points.Add(new Point3D(currentList[0]));
                topFacet.Points.Add(Points[Points.Count - 1]);
                for (int j = 1; j < currentList.Count; j++)
                {
                    currentList[j].Apply(matrix);
                    Points.Add(new Point3D(currentList[j]));
                    Facets.Add(new Facet(
                        new Point3D[] { Points[Points.Count - generatrix.Count - 2],
                                        Points[Points.Count - generatrix.Count - 1],
                                        Points[Points.Count - 1],
                                        Points[Points.Count - 2]}));
                }
                downFacet.Points.Add(Points[Points.Count - 1]);
            }

            // Добавление последного слоя граней
            for (int j = 1; j < generatrix.Count; j++)
            {
                Facets.Add(new Facet(
                        new Point3D[] { Points[Points.Count - generatrix.Count + j - 1],
                                        Points[Points.Count - generatrix.Count + j],
                                        Points[j],
                                        Points[j - 1]}));
            }
            Facets.Add(topFacet);
            Facets.Add(downFacet);
        }

        public Point3D Center
        {
            get
            {
                Point3D p = new Point3D(0, 0, 0);
                for (int i = 0; i < Points.Count; i++)
                {
                    p.X += Points[i].X;
                    p.Y += Points[i].Y;
                    p.Z += Points[i].Z;
                }
                p.X /= Points.Count;
                p.Y /= Points.Count;
                p.Z /= Points.Count;
                return p;
            }
        }

        public void Apply(Transformation t)
        {
            foreach (var point in Points)
                point.Apply(t);
        }

        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            foreach (Facet facet in Facets)
                facet.Draw(g, projection, width, height);
        }
    }


    class Plot : IPolyhedron
    {
        private List<Point3D> Points = new List<Point3D>();
        private List<Facet> Facets = new List<Facet>();
        
        public void Draw(Graphics g, Transformation projection, int width, int height)
        {
            foreach (var facet in Facets)
                facet.Draw(g, projection, width, height);
        }

        public void Apply(Transformation t)
        {
            foreach (Point3D point in Points)
                point.Apply(t);
        }

        public Point3D Center
        {
            get
            {
                double xSum = 0;
                double ySum = 0;
                double zSum = 0;
                foreach (Point3D point in points)
                {
                    xSum += point.X;
                    ySum += point.Y;
                    zSum += point.Z;
                }
                return new Point3D(xSum / points.Count, ySum / points.Count, zSum / points.Count);
            }
        }

        public List<Point3D> points { get => Points; }
        public List<Facet> facets { get => Facets; }
        public string polyhedronName { get => "Plot"; }

        public Plot() { }

        public Plot(string function, double minX, double maxX, double minZ, double maxZ, int xTicks, int zTicks)
        {
            Argument xArgument = new Argument("x");
            Argument zArgument = new Argument("z");
            Expression expression = new Expression(function, xArgument, zArgument);

            double deltaX = (maxX - minX) / xTicks;
            double deltaY = (maxZ - minZ) / zTicks;
            
            // Добавляем точки.
            double z = minZ;
            for (int i = 0; i <= zTicks; i++)
            {
                double x = minX;
                for (int j = 0; j <= xTicks; j++)
                {
                    xArgument.setArgumentValue(x);
                    zArgument.setArgumentValue(z);
                    double y = expression.calculate();
                    points.Add(new Point3D(x, y, z));

                    x += deltaX;
                }
                z += deltaY;
            }
            
            // Добавляем грани.
            for (int i = 0; i < zTicks; i++)
                for (int j = 0; j < xTicks; j++)
                    facets.Add(new Facet(new Point3D[]
                    {
                        points[i * (zTicks + 1) + j],
                        points[i * (zTicks + 1) + (j + 1)],
                        points[(i + 1) * (zTicks + 1) + (j + 1)],
                        points[(i + 1) * (zTicks + 1) + j]
                    }));
        }
    }
}