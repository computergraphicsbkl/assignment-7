﻿using ObjParser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Editor3D
{
    public partial class MainForm : Form
    {
        private List<IPrimitive> scene = new List<IPrimitive>();
        private Transformation projection;
        private IPolyhedron curPolyhedron;
        private Graphics graphics;
        private string currentFileName = "";
        private bool currentChanged = false;
        private Obj currentObj = null;
        
        private PlotForm plotForm = new PlotForm();

        public MainForm()
        {
            InitializeComponent();

            Image bitmap = new Bitmap(2000, 2000);
            graphics = Graphics.FromImage(bitmap);
            pictureBox1.Image = bitmap;

            Point3D oPoint = new Point3D(0, 0, 0);
            Point3D xPoint = new Point3D(1, 0, 0, Color.Red);
            Line xAxis = new Line(oPoint, xPoint, Color.Red);
            Point3D yPoint = new Point3D(0, 1, 0, Color.LawnGreen);
            Line yAxis = new Line(oPoint, yPoint, Color.LawnGreen);
            Point3D zPoint = new Point3D(0, 0, 1, Color.Blue);
            Line zAxis = new Line(oPoint, zPoint, Color.Blue);
            scene.AddRange(new IPrimitive[] { xPoint, xAxis, yPoint, yAxis, zPoint, zAxis });

            currentObj = new Obj();

            radioButton8_CheckedChanged(this, EventArgs.Empty);

            setContentChanged(false);
            setControlsActive(false);



        }

        private void setControlsActive(bool active = true)
        {
            numericUpDown8.Enabled = active;
            numericUpDown9.Enabled = active;
            numericUpDown20.Enabled = active;
            button3.Enabled = active;

            numericUpDown5.Enabled = active;
            numericUpDown6.Enabled = active;
            numericUpDown7.Enabled = active;
            button2.Enabled = active;

            numericUpDown1.Enabled = active;
            numericUpDown2.Enabled = active;
            numericUpDown4.Enabled = active;
            button1.Enabled = active;

            radioButton1.Enabled = active;
            radioButton2.Enabled = active;
            radioButton3.Enabled = active;
            button4.Enabled = active;

            numericUpDown21.Enabled = active;
            numericUpDown22.Enabled = active;
            numericUpDown23.Enabled = active;
            button7.Enabled = active;

            radioButton4.Enabled = active;
            radioButton5.Enabled = active;
            radioButton6.Enabled = active;
            numericUpDown19.Enabled = active;
            button5.Enabled = active;

            numericUpDown10.Enabled = active;
            numericUpDown13.Enabled = active;
            numericUpDown14.Enabled = active;
            numericUpDown15.Enabled = active;
            numericUpDown16.Enabled = active;
            numericUpDown17.Enabled = active;
            numericUpDown18.Enabled = active;
            button6.Enabled = active;

            radioButton7.Enabled = active;
            radioButton8.Enabled = active;
            radioButton9.Enabled = active;
            radioButton10.Enabled = active;
            radioButton11.Enabled = active;
            numericUpDown11.Enabled = active;
            saveToolStripMenuItem.Enabled = active;
            closeToolStripMenuItem.Enabled = active;
        }

        private void scenesRefresh()
        {
            graphics.Clear(Color.White);
            clearObjContainer();
            foreach (var primitive in scene)
                primitive.Draw(graphics, projection, pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Refresh();
            setContentChanged();
        }

        private void clearObjContainer()
        {
            currentObj.CustomList.Clear();
            currentObj.VertexList.Clear();
            currentObj.FaceList.Clear();
        }

        private void setContentChanged(bool state = true)
        {
            this.Text = "Editor3D" + (String.IsNullOrEmpty(currentFileName) ? "" : " - " + currentFileName);
            if (state)
            {
                saveToolStripMenuItem.Text = "* Сохранить";
                currentChanged = true;
                this.Text += "*";
            }
            else
            {
                saveToolStripMenuItem.Text = "Сохранить";
                currentChanged = false;
            }
        }

        private void TetrahedronButton_Click(object sender, EventArgs e)
        {
            tetrahedronToolStripMenuItem_Click(sender, e);
        }

        private void IcosahedronButton_Click(object sender, EventArgs e)
        {
            icosahedronToolStripMenuItem_Click(sender, e);

        }

        private void DodecahedronButton_Click(object sender, EventArgs e)
        {
            dodecahedronToolStripMenuItem_Click(sender, e);
        }

        // apply translation
        private void button3_Click(object sender, EventArgs e)
        {
            double dx = (double)numericUpDown8.Value;
            double dy = (double)numericUpDown9.Value;
            double dz = (double)numericUpDown20.Value;
            curPolyhedron.Apply(Transformation.Translate(dx, dy, dz));
            scenesRefresh();
        }

        // apply rotation
        private void button2_Click(object sender, EventArgs e)
        {
            double angleX = (double)numericUpDown7.Value * Math.PI / 180;
            double angleY = (double)numericUpDown5.Value * Math.PI / 180;
            double angleZ = (double)numericUpDown6.Value * Math.PI / 180;
            curPolyhedron.Apply(Transformation.RotateX(angleX));
            curPolyhedron.Apply(Transformation.RotateY(angleY));
            curPolyhedron.Apply(Transformation.RotateZ(angleZ));
            scenesRefresh();
        }

        // apply scale
        private void button1_Click(object sender, EventArgs e)
        {
            double scaleX = (double)numericUpDown4.Value;
            double scaleY = (double)numericUpDown2.Value;
            double scaleZ = (double)numericUpDown1.Value;
            curPolyhedron.Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
            scenesRefresh();
        }

        // apply reflection
        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectX());
            }
            else if (radioButton2.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectY());
            }
            else if (radioButton3.Checked)
            {
                curPolyhedron.Apply(Transformation.ReflectZ());
            }
            scenesRefresh();
        }

        // apply center scale
        private void button7_Click(object sender, EventArgs e)
        {
            Point3D center = curPolyhedron.Center;
            double scaleX = (double)numericUpDown21.Value;
            double scaleY = (double)numericUpDown22.Value;
            double scaleZ = (double)numericUpDown23.Value;
            curPolyhedron.Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            curPolyhedron.Apply(Transformation.Scale(scaleX, scaleY, scaleZ));
            curPolyhedron.Apply(Transformation.Translate(center.X, center.Y, center.Z));
            scenesRefresh();

        }

        // apply center rotation
        private void button5_Click(object sender, EventArgs e)
        {
            Point3D center = curPolyhedron.Center;
            double angle = (double)numericUpDown19.Value * Math.PI / 180;
            curPolyhedron.Apply(Transformation.Translate(-center.X, -center.Y, -center.Z));
            if (radioButton4.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateX(angle));
            }
            else if (radioButton5.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateY(angle));
            }
            else if (radioButton6.Checked)
            {
                curPolyhedron.Apply(Transformation.RotateZ(angle));
            }
            curPolyhedron.Apply(Transformation.Translate(center.X, center.Y, center.Z));
            scenesRefresh();
        }

        private double getAngle(double sin, double cos)
        {
            if (cos >= 0)
                return Math.Asin(sin);
            else
                return Math.PI * Math.Sign(sin) - Math.Asin(sin);
        }

        // apply line roll
        private void button6_Click(object sender, EventArgs e)
        {
            Line line = new Line(
                new Point3D((double)numericUpDown13.Value,
                    (double)numericUpDown14.Value,
                    (double)numericUpDown15.Value),
                new Point3D((double)numericUpDown10.Value,
                    (double)numericUpDown17.Value,
                    (double)numericUpDown18.Value));

            scene.Add(line);

            Transformation translate = Transformation.Translate(-line.A.X, -line.A.Y, -line.A.Z);
            Transformation reverseTranslate = Transformation.Translate(line.A.X, line.A.Y, line.A.Z);
            curPolyhedron.Apply(translate);
            line.Apply(translate);
            // Исключить случай, когда линия лежит на оси Oy.
            bool isOnOy = Math.Abs(line.B.X) <= double.Epsilon && Math.Abs(line.B.Z) <= double.Epsilon;
            Transformation reverseRotateY = new Transformation();
            Transformation reverseRotateZ = new Transformation();
            if (!isOnOy)
            {
                double lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Z * line.B.Z);
                double sin = line.B.Z / lineProjectionLength;
                double cos = line.B.X / lineProjectionLength;
                double lineProjectionAngle = getAngle(sin, cos);
                Transformation rotateY = Transformation.RotateY(-lineProjectionAngle);
                reverseRotateY = Transformation.RotateY(lineProjectionAngle);
                curPolyhedron.Apply(rotateY);
                line.Apply(rotateY);
                lineProjectionLength = Math.Sqrt(line.B.X * line.B.X + line.B.Y * line.B.Y);
                sin = line.B.X / lineProjectionLength;
                cos = line.B.Y / lineProjectionLength;
                lineProjectionAngle = getAngle(sin, cos);
                Transformation rotateZ = Transformation.RotateZ(-lineProjectionAngle);
                reverseRotateZ = Transformation.RotateZ(lineProjectionAngle);
                curPolyhedron.Apply(rotateZ);
                line.Apply(rotateZ);
            }
            double angle = (double)numericUpDown16.Value * Math.PI / 180;
            Transformation rotate = Transformation.RotateY(angle);
            curPolyhedron.Apply(rotate);
            if (!isOnOy)
            {
                curPolyhedron.Apply(reverseRotateZ * reverseRotateY);
                line.Apply(reverseRotateZ * reverseRotateY);
            }
            curPolyhedron.Apply(reverseTranslate);
            line.Apply(reverseTranslate);
            scenesRefresh();
            scene.Remove(line);
        }

        // perspective
        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton8.Checked)
            {
                double fov = (double)numericUpDown11.Value;
                projection = Transformation.RotateY(Math.PI / 4)
                             * Transformation.RotateX(-Math.PI / 4)
                             * Transformation.Translate(0, 0, -1)
                             * new Transformation(new double[,]
                             {
                                 {1, 0, 0, 0},
                                 {0, 1, 0, 0},
                                 {0, 0, 0, -fov},
                                 {0, 0, 0, 1}
                             });
            }
            else if (radioButton7.Checked)
                projection = Transformation.RotateY(Math.PI / 4)
                             * Transformation.RotateX(-Math.PI / 4);
            else if (radioButton9.Checked)
                projection = Transformation.Identity();
            else if (radioButton10.Checked)
                projection = Transformation.RotateY(Math.PI / 2);
            else if (radioButton11.Checked)
                projection = Transformation.RotateX(-Math.PI / 2);
            scenesRefresh();
        }

        private void pictureBox1_Resize(object sender, EventArgs e)
        {
            if (graphics != null)
                scenesRefresh();
        }

        private void numericUpDown11_ValueChanged(object sender, EventArgs e)
        {
            radioButton8_CheckedChanged(this, EventArgs.Empty);
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filePath = openFileDialog1.FileName;
                    using (Stream str = openFileDialog1.OpenFile())
                    {
                        currentFileName = filePath;
                    }
                }
                catch (SecurityException ex)
                {
                    MessageBox.Show($"Security error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
                if (curPolyhedron != null)
                    scene.Remove(curPolyhedron);
                currentObj = new Obj();
                currentObj.LoadObj(currentFileName);

                string polyhedron_name = currentObj.CustomList[0];
                bool okay = false;
                switch (polyhedron_name)
                {
                    case "Tetrahedron":
                        curPolyhedron = new Tetrahedron();
                        okay = true;
                        break;
                    case "Icosahedron":
                        curPolyhedron = new Icosahedron();
                        okay = true;
                        break;
                    case "Dodecahedron":
                        curPolyhedron = new Dodecahedron();
                        okay = true;
                        break;
                    case "RotationFigure":
                        curPolyhedron = new RotationFigure();
                        okay = true;
                        break;
                    case "Plot":
                        curPolyhedron = new Plot();
                        okay = true;
                        break;
                    default:
                        MessageBox.Show("File cannot be opened with this editor. Polyhedron isn't recognised.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                if (okay)
                {
                    //MessageBox.Show("Info.", "Info!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    foreach (ObjParser.Types.Vertex vert in currentObj.VertexList)
                    {
                        // MessageBox.Show(vert.ToString(), "TEST");
                        Point3D point = new Point3D(vert.X, vert.Y, vert.Z);
                        // MessageBox.Show(point.ToString(), "TEST");
                        curPolyhedron.points.Add(point);
                    }

                    foreach (ObjParser.Types.Face face in currentObj.FaceList)
                    {
                        Facet facet = new Facet();
                        foreach (int index in face.VertexIndexList)
                            facet.Points.Add(curPolyhedron.points[index - 1]);
                        curPolyhedron.facets.Add(facet);
                    }
                    scene.Add(curPolyhedron);
                    scenesRefresh();
                    setContentChanged(false);
                    setControlsActive();
                }

            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (!String.IsNullOrEmpty(currentFileName))
                saveFileDialog1.FileName = saveFileDialog1.FileName;

            if (saveFileDialog1.FileName != "")
            {
                string[] headers = new string[] { curPolyhedron.polyhedronName };

                currentObj.CustomList.Add("o " + curPolyhedron.polyhedronName);
                int i = 0;
                foreach (Facet f in curPolyhedron.facets)
                {

                    string[] idsList = new string[f.Points.Count + 1];
                    idsList[0] = "f";
                    ObjParser.Types.Face face = new ObjParser.Types.Face();

                    for (int j = 0; j < f.Points.Count; j++)
                    {
                        //MessageBox.Show("i=" + i+" j="+j, "TEST");

                        Point3D p = f.Points[j];
                        ObjParser.Types.Vertex v = new ObjParser.Types.Vertex();
                        //MessageBox.Show(string.Join("/", p.ToStringArray()), "TEST");
                        v.LoadFromStringArray(p.ToStringArray());
                        int idx = currentObj.VertexList.FindIndex(
                            vert=>(vert.X.Equals(v.X)&&vert.Y.Equals(v.Y)&&vert.Z.Equals(v.Z)));
                        if (idx == -1)
                        {
                            i++;
                            v.Index = i;
                            currentObj.VertexList.Add(v);
                        }
                        else
                        {
                            v.Index = currentObj.VertexList[idx].Index;
                        }
                        
                        idsList[1 + j] = v.Index.ToString();
                    }
                    //MessageBox.Show(string.Join("/", idsList), "TEST");
                    face.LoadFromStringArray(idsList);
                    currentObj.FaceList.Add(face);
                }
                currentFileName = saveFileDialog1.FileName;
                currentObj.WriteObjFile(currentFileName, headers);
                clearObjContainer();
                setContentChanged(false);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private bool doublesEqual(double d1, double d2, double tolerance = 0.00001)
        {
            return (Math.Abs(d1 - d2) < tolerance);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentFileName = "";
            curPolyhedron = null;
            currentObj = new Obj();
            scene.Clear();
            setControlsActive(false);
            setContentChanged(false);
            scenesRefresh();
        }

        private void tetrahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createIPolyhedron("Tetrahedron");

        }

        private void icosahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createIPolyhedron("Icosahedron");

        }

        private void dodecahedronToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createIPolyhedron("Dodecahedron");
        }

        private void createIPolyhedron(string polyhedron_name)
        {
            switch (polyhedron_name)
            {
                case "Tetrahedron":
                    scene.Remove(curPolyhedron);
                    curPolyhedron = new Tetrahedron((double)numericUpDown12.Value);
                    scene.Add(curPolyhedron);
                    break;
                case "Icosahedron":
                    scene.Remove(curPolyhedron);
                    curPolyhedron = new Icosahedron((double)numericUpDown12.Value);
                    scene.Add(curPolyhedron);
                    break;
                case "Dodecahedron":
                    scene.Remove(curPolyhedron);
                    curPolyhedron = new Dodecahedron((double)numericUpDown12.Value);
                    scene.Add(curPolyhedron);
                    Dodecahedron dod = new Dodecahedron((double)numericUpDown12.Value);
                    break;
                case "RotationFigure":
                    scene.Add(curPolyhedron);
                    break;
                case "Plot":
                    if (plotForm.ShowDialog() != DialogResult.OK)
                        break;
                    scene.Remove(curPolyhedron);
                    string function = plotForm.textBox1.Text;
                    double minX = (double) plotForm.numericUpDown1.Value;
                    double maxX = (double) plotForm.numericUpDown2.Value;
                    double minY = (double) plotForm.numericUpDown3.Value;
                    double maxY = (double) plotForm.numericUpDown4.Value;
                    int xTicks = (int) plotForm.numericUpDown5.Value;
                    int yTicks = (int) plotForm.numericUpDown6.Value;
                    curPolyhedron = new Plot(function, minX, maxX, minY, maxY, xTicks, yTicks);
                    scene.Add(curPolyhedron);
                    break;
                default:
                    MessageBox.Show("Unexpected error. Polyhedron isn't recognised.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }

            currentFileName = "";
            scenesRefresh();
            setControlsActive();
        }

        private void plotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            createIPolyhedron("Plot");
        }

        private void rotationFigureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RotationFigureForm form = new RotationFigureForm(this);
            form.Show();

        }

        public void DrawRotationFigure(List<Point3D> points, Transformation matrix, int splits)
        {
            RotationFigure figure = new RotationFigure(points, matrix, splits);
            System.Diagnostics.Debug.WriteLine($"Figure Points size: {figure.Points.Count}");
            System.Diagnostics.Debug.WriteLine($"Figure Facets size: {figure.Facets.Count}");

            scene.Remove(curPolyhedron);
            curPolyhedron = figure;
            createIPolyhedron("RotationFigure");
        }
    }
}